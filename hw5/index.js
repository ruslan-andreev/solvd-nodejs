//Task1
const numbersTask1 = [1, 2, 3, 4, 5, 1, 4, 7, 2];
const usersArr = [
  {id: 1, name: "Alex"},
  {id: 2, name: "Bob"},
  {id: 3, name: "Kate"},
  {id: 4, name: "Ivan"},
  {id: 5, name: "Alex"},
  {id: 6, name: "Ivan"},
  {id: 7, name: "Kate"},
  {id: 8, name: "Bob"},
];
  
function customFilterUnique(arr, callback) {

  if(!arr || arr.length === 0) return new Error("Wrong data or empty array!");
  if(!callback) return new Error("Enter callback function!");

  const uniqueObj = new Set();

  for(let i = 0; i < arr.length; i++) {
    let element = arr[i];

    let isTrue = callback(element, arr);

    if(isTrue && typeof element !== "object"){
      if(!uniqueObj.has(element) && isTrue){
        uniqueObj.add(element);
      } 
    }else if(isTrue && typeof element == "object"){
      if(!uniqueObj.has(element.name) && isTrue) {
        uniqueObj.add(element.name, element)
      }
    }
  }
  return [...uniqueObj];
}

function filter(element, arr) {
  return arr.indexOf(element) === arr.lastIndexOf(element);
}
function filterEven(element) {
  return element % 2 === 0;
}

function filterObjects(obj) {
  if(obj.id % 2 === 0){
    return obj;
  }
}

try {
  console.log(customFilterUnique(numbersTask1, filter));
  console.log(customFilterUnique(numbersTask1, filterEven));
  console.log(customFilterUnique(usersArr, filterObjects));
} catch (error) {
  console.log(error.massage);
}

//Task2.1
const arr = [1, 2, 3, 4, 5, 6, 7];

function chunkArray(arr, chunk) {

  try {
    if(!arr || arr.length === 0) throw new Error("Array is empty!");
    if(!chunk || typeof chunk !== 'number') throw new Error("Wrong chunk value!");

    const chunkArr = [];
    for (let i = 0; i < arr.length; i += chunk) {
      chunkArr.push(arr.slice(i, i + chunk));
    }
    return chunkArr;
    
  } catch (error) {
    return error;
  }
}

console.log(chunkArray(arr, 3));

//Task2.2
function chunkArray2(arr, chunk) {
  try {
    if(!arr || arr.length === 0) throw new Error("Array is empty!");
    if(!chunk || typeof chunk !== 'number') throw new Error("Wrong chunk value!");

    const chunkArr = [];
    while (arr.length > 0) {
      chunkArr.push(arr.splice(0, chunk));
    }
  
    return chunkArr;
  } catch (error) {
    return error
  }
  
}
//console.log(chunkArray(arr, 3))

//Task3
const arrTask3 = ['apple', 'banana', 'orange'];

function customShuffle(arr) {
  try {
    if(!Array.isArray(arr) || arr.length === 0) throw new Error("Wrong data!");

    for (let i = arr.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));

    [arr[i], arr[j]] = [arr[j], arr[i]];
  }
    return arr;
  } catch (error) {
    return error;
  }
  
}

console.log(customShuffle(arrTask3));

//Task4.1
const arrForTask4a = [1, 2, 2, 4, 6, 7, 7];
const arrForTask4b = [2, 3, 3, 4, 5, 6, 7, 7, 8];

function getArrayIntersection(arr1, arr2) {
  const arrIntersection = [];
  const arr1Uniq = [...new Set(arr1)];
  const arr2Uniq = [...new Set(arr2)];

  for(let elem of arr1Uniq) {
    if(arr2Uniq.includes(elem)){
      arrIntersection.push(elem);
    }
  }
  return arrIntersection;

}
//console.log(getArrayIntersection(arrForTask4a, arrForTask4b))

//Task4.2
function getArrayUnion(arr1, arr2) {
  try {
    if(!Array.isArray(arr1) || !Array.isArray(arr1)) throw new Error("Wrong data type!")
    return [...new Set([...arr1, ...arr2])];
  } catch (error) {
    return error;
  }
  
}
console.log(getArrayUnion(arrForTask4a, arrForTask4b));

//Task5
function measureArrayPerformance(fn, arr, callback) {
  const startDate = performance.now();
  
  fn(arr, callback);

  const endDate = performance.now();
  
  const perfomanceTyme = endDate - startDate;

  return `Funktion perfomance is ${perfomanceTyme} ms`;
}

console.log(measureArrayPerformance(customFilterUnique, numbersTask1, filter));
//Funktion perfomance is 0.10049986839294434 ms

//Оказалось самописный фильтр быстрее чем Set
function filterWithSet(arr) {
  return [...new Set([...arr])];
}
console.log(measureArrayPerformance(filterWithSet, numbersTask1));
//Funktion perfomance is 0.13719987869262695 ms
