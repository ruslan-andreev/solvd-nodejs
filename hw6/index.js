//Task1
const translations = {  
    en: {  
    greet: "Hello",  
    intro: "Welcome to our website"  
  },  
    fr: {  
    greet: "Bonjour",  
    intro: "Bienvenue sur notre site web"  
  }  
};

const language = 'fr';
const greeting = 'greet';
const introduction = 'intro';

function localize(str, ...values) {
  const value = values[0];
  const language = values[1];

  try {
    if(language !== 'fr' && language !== 'en') throw new Error("Language is not available!");
    if(language === undefined) throw new Error("Chuse language!");

    return str = translations[language][value];
  } catch (error) {
    return error;
  }
  
};

const localizedGreeting = localize`${greeting}, ${language}`;
const localizedIntroduction = localize`${introduction}, ${language}`;

//console.log(localizedGreeting);
//console.log(localizedIntroduction);

//Task2
const keywords = ["JavaScript", "template", "tagged"];
const template = "Learn \${0} tagged templates to create custom \${1} literals for \${2} manipulation.";


function highlightKeywords(template, keywords) {
  try {
    if(!template || !keywords) return new Error('Wrong arguments!');
    
    return template.replace(/\${(\d+)}/g, (match, index) => {
      const keyword = keywords[index];
      return `<span class="highlight">${keyword}</span>`;
  
    });
    
  } catch (error) {
    return error;
  }

}
const highlighted = highlightKeywords(template, keywords);
//console.log(highlighted)
 
//Expected: "Learn <span class='highlight'>JavaScript</span> tagged templates to create custom <span class='highlight'>template</span> literals for <span class='highlight'>tagged</span> manipulation."

//Task3
function multiline(string) {
  
  try {
    if(!string) throw new Error("Wrong arguments!");
    if(!Array.isArray(string) || string.length === 0) throw new Error('Wrong data type or empty!');

    const strArr = string.join('').split('\n');
    const result = [];

    for(let i = 0; i < strArr.length; i++) {
      if(strArr[i] === '') continue;
      let counter = i;
      let string = `${counter} ${strArr[i]}`;
      result.push(string);
    }

    return result.join('\n');
    
  } catch (error) {
    return error;
  }
  
}

const code = multiline`  
    function add(a, b) {  
    return a + b;  
  }  
`;  
  
console.log(code);  

//task 6

function multiply(a, b, c) {
  return a * b * c;  
}

function curry(fn, arity) {
  try {
    
    if(arity <= 0) throw new Error("Wrong 'arity' value!");
    if(typeof fn !== "function") throw new Error("Enter callback function!");
    
    return function curFunction(...arg) {
      if(arg.length >= arity){
        return fn(...arg);
      }else{
        return (...nextArg) => {
          return curFunction(...arg, ...nextArg);
        }
        
      }
    } 

  } catch (error) {
    return error;
  }
  
}
    
const curriedMultiply = curry(multiply, 3);  
    
const step1 = curriedMultiply(2); // Returns a curried function  
const step2 = step1(3); // Returns a curried function  
const result = step2(4); // Returns the final result: 2 * 3 * 4 = 24  
    
console.log("Result:", result); // Expected: 24
