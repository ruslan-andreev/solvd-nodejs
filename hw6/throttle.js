function throttle(fn, timeout) {
  try {
    if(typeof fn !== "function") throw new Error("Enter callback function!");
    if(timeout <= 0 || typeof timeout != "number" || Number.isNaN(timeout)){
      throw new Error("Wrong timeout value!")
    } 

    let timer = null;
    return (...arg) => {
      if(timer) return;
      
      timer = setTimeout(() => {
        fn(arg);
        clearTimeout(timer);
        timer = null;
      }, timeout);
    }
  } catch (error) {
    return error;
  }
  
}

function onScroll(event) {  
  // Handle scroll event  
  console.log("Scroll event:", event);  
}  
    
const throttledScrollHandler = throttle(onScroll, 1000);  
    
window.addEventListener("scroll", throttledScrollHandler);