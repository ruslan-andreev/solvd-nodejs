function debounce(fn, timeout) {
  try {
    if(typeof fn !== "function") throw new Error("Enter callback function!");
    if(timeout <= 0 || typeof timeout != "number" || Number.isNaN(timeout)){
      throw new Error("Wrong timeout value!")
    } 

    let timer;
    return (...arg) =>{

    clearTimeout(timer);
    timer = setTimeout(() => {
      fn(arg)
      }, timeout);
    }
    
  } catch (error) {
    return error;
  }
  
}

function debouncedSearch(query) {  
  // Perform search operation with the query  
  console.log("Searching for:", query);  
}  
    
const debouncedSearchHandler = debounce(debouncedSearch, 300);  

try {
  const inputElement = document.getElementById("search-input");  
  if(!inputElement) throw new Error('Input is Undefined!');

  inputElement.addEventListener("input", event => {  
  debouncedSearchHandler(event.target.value);  
});
} catch (error) {
  throw error;
} 
