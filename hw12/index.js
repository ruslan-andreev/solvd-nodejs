class NodeForLinkedList {
  constructor(key, data, next = null) {
    this.key = key;
    this.data = data;
    this.next = next;
  }
}

class LinkedList {
  head = null;
  
  inserting(key, data) {
    
    const newNode = new NodeForLinkedList(key, data);
    
    if (this.head === null) {
      this.head = newNode;
    } else {
      let current = this.head;
      while (current.next !== null) {
        current = current.next;
      }
      current.next = newNode;
    }
    
    return this;
  }
  
  deleting(key) {
    
    if (this.head === null) {
      return false;
    }
    
    if(this.head.key === key){
      this.head = this.head.next;
      return true;
    }
    let current = this.head;
    let previous = null;
    
    while (current !== null && current.key !== key) {
      previous = current;
      current = current.next;
    }
    if (current === null) {
      return false;
    }
    previous.next = current.next;
    return true;
  }
  
  searching(key) {
    let current = this.head;
    
    while (current !== null) {
      if (current.key === key) {
        return current.data;
      }
      current = current.next;
    }
    return false;
  }
 
}

const checkKey = (key) => {
  if(!key) throw new Error("Key is udefined, enter Key");
  if(typeof key !== "string") throw new Error("Wrond Key type, enter a string");
}

const checkData = (key, value) => {
  if(!key) throw new Error("Key is udefined, enter Key");
  if(typeof key !== "string") throw new Error("Wrond Key type, enter a string");
  if(!value) throw new Error("Value is undefined, enter value");
}
class CustomHashTable {
  numItems = 0;
  loadFactor = null; // вероятность коллизий, если больше 0.7
                    //resize hashTable в x2
  constructor(size) {
    if(!size) throw new Error("Enter table size");
    if(typeof size !== "number" || Number.isNaN(size)) throw new Error("Wrong data type, enter number");
    this.hashTableSize = size;
    this.hashTable = new Array(this.hashTableSize);
  }
  //hash function  
  hash(key, length) {
    let chareCode = [];
    let hashKey = null;
    for(let i = 0; i < key.length; i++){
      chareCode.push(key.charCodeAt(i));
    }
    hashKey = chareCode.reduce((acc, val) => acc + val, 0) % length;
    return hashKey;
  }
  //в случае если таблица мала и вероятность коллизий > 0.7 увеличиваем таблицу х2
  //и переиндексируем едементы, не эффективно (сложность O(n))
  resizeTable() {
    const newHashTable = new Array(this.hashTable.length * 2);
    this.hashTable.forEach((element) => {
      if(element) {//если element есть, переиндексируем все элементы LinkedList и
                  // помещаем в новый массив, если возникают коллизии, добавляем в LinkedList
        let current = element.head;
        while(current !== null) {
          const newIndex = this.hash(current.key, newHashTable.length);
          if(!newHashTable[newIndex]) {
            newHashTable[newIndex] = new LinkedList();
          }
          newHashTable[newIndex].inserting(current.key, current.data)
          current = current.next;
        }
      }
    })
      this.hashTable = [...newHashTable];
      this.hashTableSize = this.hashTable.length;
  }
  //добавляем элемент в таблицу.
  insert(key, value) {
    checkData(key, value)
    this.numItems++;
    const index = this.hash(key, this.hashTable.length);//метод возвращает hash(индекс для добавления в таблицу)
    
    this.loadFactor = this.numItems / this.hashTableSize;//выражение возвращает вероятность коллизий
    
    if(this.loadFactor > 0.7) {
      this.resizeTable() //если вероятность коллизий > 0.7, значит таблица имеет малое кол-во ячеек
                         //вызываем метод resizeTable который увеличивает таблицу в двое и переиндексирует элементы
    }
    if(!this.hashTable[index]) {//если в таблице нет индекса(хеша), то добавляем его и записываем LinkedList
      this.hashTable[index] = new LinkedList();
    }
    this.hashTable[index].inserting(key,value);//в LinkedList добавляем ноду в которой ханится Data
                                        //если индекс уже занят, то в LinkedList добавится ноавя нода 
  }
  //получаем Data элемента по ключу
  get(key) {
    checkKey(key);
    const index = this.hash(key, this.hashTable.length);//из ключа получаем hash
    if(!this.hashTable[index]) throw new Error("Key is undefined");//если нет ключа выбрасываем ошибку
    if(this.hashTable[index].searching(key)) {//если ключ есть, то у LinkedList вызываем метод searching
                                      //который ищет по key едемент в LinkedList
                                      //(итерируясь по head и сравнива head с key)
      return this.hashTable[index].searching(key);//возвращаем Data
    } else {
      return `Key ${key} is undefined`;
    }
  }

  delete(key) {
    checkKey(key);
    this.numItems--;
    const index = this.hash(key, this.hashTable.length);//из ключа получаем hash
    
    if(!this.hashTable[index]) throw new Error("Key is undefined");//если нет ключа выбрасываем ошибку
    const response = this.hashTable[index].deleting(key);//вызываем метод deleting у LinkedList

   if(this.hashTable[index].head === null){//если в LinkedList элемент был один, то его head == null
     this.hashTable[index] = null;//удаляем ячейку из таблицы
   }      
                                            
    if(response) {//проверяем response от метода deleting
      this.numItems--;
      return `Key ${key} deleted`; //если Key был найден, то удаляем ноду, или пререзаписываем head на null
                                  //если элементоа несколько в ноде, то меняем указатель next и возвращаем true
    }
    return `Key ${key} is undefined`;//если данного ключа нет в LinkedList возвращаем false
  }

  getAllData() {
    let dataList = '';
    this.hashTable.forEach(element => {//итерируемя по таблице
      if(element) {//если есть element присваиваем current head элемента LinkedList
        let current = element.head;
        while(current !== null) {
        dataList += `Name: ${current.key}, data: ${current.data}; `
        current = current.next;//приваиваем current указатель next(на следующий элемент) и продолжаем цикл
        }                      //пока не будет null и добавляем Data строку dataList
      }
    })
    return dataList;
  }
}

const myHashTable = new CustomHashTable(10);

myHashTable.insert("Alex", "+375 66 666 66 66");
myHashTable.insert("Bob", "+375 33 333 33 33");
myHashTable.insert("Kate", "+375 25 555 55 55");
myHashTable.insert("Anna", "+375 22 222 22 22");
myHashTable.insert("John", "+375 44 444 44 44");

//console.log(myHashTable.get("Alex")); //will return Alex number +375 66 666 66 66
//console.log(myHashTable.get("Bob")); //will return Bob number +375 33 333 33 33
//console.log(myHashTable.get("Bbbb")); //will return Key is undefined
//console.log(myHashTable.delete('Alex')); //will return Key Alex deleted
//console.log(myHashTable.get("Anna")); //will return Anna number +375 22 222 22 22
//console.log(myHashTable.delete('John')) //will return Key John deleted
//console.log(myHashTable.delete('Anna')) //will return Anna deleted
//console.log(myHashTable.get("John")); //will return Key is undefined after delited
//console.log(myHashTable.getAllData()) // will return string of data

console.log(JSON.stringify(myHashTable));
