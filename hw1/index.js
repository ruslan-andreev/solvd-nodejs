class String {
  constructor (string) {
    this.string = string;
  }

  multiply(str2) {
    const ArrLength = this.string.length + str2.length;
    const result = new Array(ArrLength).fill(0);
  
    for(let i = this.string.length -1; i >= 0; i--) {
      for(let j = str2.length -1; j >= 0; j--) {
  
        let index1 = i + j + 1;
        let index2 = i + j;
        
        let tempValue = result[index1] + +this.string[i] * +str2[j];
  
        result[index1] = tempValue % 10;
        result[index2] += Math.floor(tempValue / 10);
      }
    }
    return result[0] == 0 ? result.slice(1).join('') : result.join('');
  }

  plus(str2) {
    let str1length = this.string.length - 1;
    let str2length = str2.length - 1;
  
    let tempValue = 0;
    let result = [];
    while(str1length >= 0 || str2length >= 0) {
      let n1 = 0;
      let n2 = 0;
  
      if(str1length >= 0){
        n1 = this.string[str1length]
      }
      if(str2length >= 0){
        n2 = str2[str2length]
      }
  
      let sum = +n1 + +n2 + tempValue;
   
    if(sum >= 10) {
      result.push(sum % 10)
      tempValue = Math.floor(sum / 10);
    } else {
      result.push(sum)
      tempValue = 0;
    }
    str1length--;
    str2length--;
  }
  
  return result.reverse().join('');

  }

  minus(str2) {
    let result = "";
    let tempValue = 0;
  
    for (let i = this.string.length - 1, j = str2.length - 1; i >= 0 || j >= 0; i--, j--) {
      const digitA = i >= 0 ? +(this.string[i]) : 0;
      const digitB = j >= 0 ? +(str2[j]) : 0;
  
      let difference = digitA - digitB - tempValue;
      if (difference < 0) {
        difference += 10;
        tempValue = 1;
      } else {
        tempValue = 0;
      }
      result = difference.toString() + result;
    }
  
    while (result[0] === "0" && result.length > 1) {
      result = result.slice(1);
    }
  
    return result;
  }
  
  divide(str2) {
  
    const dividendArray = this.string.split('');
    const divisorArray = str2.split('');
  
    let result = '';
    let remainder = 0;
    for (let i = 0; i < dividendArray.length; i++) {
      const digit = parseInt(dividendArray[i]) + remainder * 10;
      const quotientDigit = Math.floor(digit / parseInt(divisorArray[0]));
      remainder = digit % parseInt(divisorArray[0]);
      result += quotientDigit;
    }
  
    result = result.replace(/^0+/, '');
  
    return result;
  }
  
}

const string = new String('1233627484');
console.log(string.multiply('56999587362382623284584514'));
console.log(string.plus('85538553785'));
console.log(string.minus("123456789"));
console.log(string.divide('948'));

