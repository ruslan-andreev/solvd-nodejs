const addValues = require('./modules/addValues');
const stringifyValue = require('./modules/stringifyValue');
const convertBoolean = require('./modules/convertBoolean');
const convertToNumber = require('./modules/convertToNumber');
const coerceToType = require('./modules/coerceToType');

function AddValues (arg1, arg2) {
  return addValues(arg1, arg2);
}

function StringifyValue(arg1, arg2) {
  return stringifyValue(arg1, arg2);
}

function ConvertBoolean(arg1, arg2) {
  return convertBoolean(arg1, arg2);
}
function ConvertToNumber(arg1) {
  return convertToNumber(arg1);
}
function CoerceToType(arg1, arg2) {
  return coerceToType(arg1, arg2);
}

module.exports = {
  AddValues,
  StringifyValue,
  ConvertBoolean,
  ConvertToNumber,
  CoerceToType
}


