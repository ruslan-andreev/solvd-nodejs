const convertToNumber = (arg) => {
 try {
  switch (typeof arg) {
    case 'number':
      return arg;
    case 'string':
      let tempVal = parseFloat(arg);
      if(Number.isNaN(tempVal) == true){
        throw Error("Cann not be converted to Number!");
      }
      return tempVal;
    default:
      throw Error("Cann not be converted to Number!");
  }
 } catch (err) {
  return err.message;
 }

}

module.exports = convertToNumber;