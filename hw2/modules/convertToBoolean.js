const convertToBoolean = (arg) => {

  try {
    let result = Boolean(arg);
    if(typeof result !== 'boolean') {
      throw Error('Cann not be converted to Boolean!');
    }

    return result;

  } catch (err) {
    return err.message;
  }
}

module.exports = convertToBoolean;