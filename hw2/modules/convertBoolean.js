const convertBoolean = (arg) => {
  
  try {
    if(typeof arg !== 'boolean'){
      throw Error("Not a Boolean value!");
    }

    return !arg;
    
  } catch (err) {
    return err.message;
  }
}

module.exports = convertBoolean;