const stringifyValue = require('./stringifyValue');
const convertToNumber = require('./convertToNumber');
const convertToBoolean = require('./convertToBoolean');

const coerceToType = (arg, type) => {
  try {
    switch (type) {
      case 'string':
        return stringifyValue(arg);

      case 'number':
        return convertToNumber(arg);

      case 'boolean':
        return convertToBoolean(arg);
    
      default:
        break;
    }
    
  } catch (err) {
    return err.message;
  }
}
module.exports = coerceToType;