const stringifyValue = (arg) => {
  
  try {
    switch (typeof arg) {

      case 'object':
        if(arg === null){         
          throw Error("Can't convert null to string!");
        }
        return JSON.stringify(arg);

      default:
        return String(arg);
    }
    
  } catch (err) {
    return err.message;
  }
}
module.exports = stringifyValue;