const addValues = (arg1, arg2) => {
  
  try{
    
    if (typeof arg1 === 'number' && typeof arg2 === 'number') {
      return arg1 + arg2;
    } else if (typeof arg1 === 'string' && typeof arg2 === 'string') {
      return arg1 + arg2;
    } else if (typeof arg1 === 'boolean' && typeof arg2 === 'boolean') {
      return arg1 || arg2;
    } else {
      throw Error('Cannot be added!');
    }
    
  } catch (err) {
    return err.message;
  }
}

module.exports = addValues;
