function createSortedArrN(N) {
  const sortedArray = [];
  for(let i = 1; i <= N; i++) {
    sortedArray.push(i);
  }
  return sortedArray;
}

function createRandomNuberN(N, arr=[]) {
  if(arr.length === N) {
    return arr;
  }
  const randomNumber = Math.floor(Math.random() * N) + 1;
  if(!arr.includes(randomNumber)) {
    arr.push(randomNumber)
  }
  return createRandomNuberN(N, arr)
}

const randomArr = createRandomNuberN(500, arr=[])

function createSortedBackwardArrN(N) {
  const sortedArray = [];
  for(let i = N; i >= 1; i--) {
    sortedArray.push(i);
  }
  return sortedArray;
}


/**BuubleSort algorithm */
function bubbleSort(arr) {
  if(!arr) throw new Error("Enter arguments!");
  if(!Array.isArray(arr) || arr.length === 0 ) throw new Error("Wrong data, or data type!");

  let swapped = false;
  for(let i = 0; i < arr.length; i++) {
    
    for(let j = 0; j < arr.length - 1; j++) {
      
      if(arr[j] > arr[j + 1]) {
        [arr[j], arr[j + 1]] = [arr[j + 1], arr[j]];
        swapped = true;
      }

    }
    if(!swapped) break;
  }
  return arr;
}
//console.log(`BubbleSort ${bubbleSort(randomArr)}`)

/**QuickSort algorithm */
function quickSort(arr) {
  if(!arr) throw new Error("Enter arguments!");
  
  if (arr.length <= 1) {
    return arr;
  }

  const pivotIndex = Math.floor(Math.random() * arr.length);
  const pivot = arr[pivotIndex];
  const left = [];
  const right = [];

  for (let i = 0; i < arr.length; i++) {
    if (i === pivotIndex) {
      continue;
    }
    if (arr[i] < pivot) {
      left.push(arr[i]);
    } else {
      right.push(arr[i]);
    }
  }

  return [...quickSort(left), pivot, ...quickSort(right)];
}
//console.log(`QuickSort ${quickSort(randomArr)}`)

/**MergSort algorithm */
function mergeSort(arr) {
  if(!arr) throw new Error("Enter arguments!");
  if(!Array.isArray(arr) || arr.length === 0 ) throw new Error("Wrong data, or data type!");

  if (arr.length <= 1) {
    return arr;
  }
  
  const mid = Math.floor(arr.length / 2);
  const left = arr.slice(0, mid);
  const right = arr.slice(mid);
  
  return merge(mergeSort(left), mergeSort(right));
}

function merge(left, right) {
  let result = [];
  let leftIndex = 0;
  let rightIndex = 0;
  
  while (leftIndex < left.length && rightIndex < right.length) {
    if (left[leftIndex] < right[rightIndex]) {
      result.push(left[leftIndex]);
      leftIndex++;
    } else {
      result.push(right[rightIndex]);
      rightIndex++;
    }
  }
  
  return result.concat(left.slice(leftIndex)).concat(right.slice(rightIndex));
}
//console.log(`MergeSort ${mergeSort(randomArr)}`)


function perfomanceTest(fn) {
  if(!fn) throw new Error("Enter function as argument!");

  return (arg) => {
    if(!arg) throw new Error("Enter arguments!");
    if(!Array.isArray(arg) || arg.length === 0 ) throw new Error("Enter data to perfopmanceTest function!");
    let array = []
    for(let i = 0; i < 10; i++){
      const startDate = performance.now();
  
      fn(arg);

      const endDate = performance.now();
      const perfomanceTime = endDate - startDate;
      array.push(perfomanceTime)
    }
    
    const averageValue = array.reduce((acc,item)=> acc + item, 0) / array.length;
    
    return `function perfomance is ${averageValue} ms`;
  }
}

const perfomanceBubbleSort = perfomanceTest(bubbleSort);
const perfomanceQuickSort = perfomanceTest(quickSort);
const perfomanceMergSort = perfomanceTest(mergeSort);

console.log(`QuickSort with random array ${perfomanceQuickSort(randomArr)}`);
//console.log(`MargeSort with random array ${perfomanceMergSort(randomArr)}`);
//console.log(`BubbleSort with random array ${perfomanceBubbleSort(randomArr)}`);

//console.log(`BubbleSort with sorted array${perfomanceBubbleSort(createSortedArrN(1000))}`);
//console.log(`QuickSort with sorted array${perfomanceQuickSort(createSortedArrN(1000))}`);
//console.log(`MargeSort with sorted array${perfomanceMergSort(createSortedArrN(1000))}`);

//console.log(`BubbleSort with backward array ${perfomanceBubbleSort(createSortedBackwardArrN(1000))}`);
//console.log(`QuickSort with backward array ${perfomanceQuickSort(createSortedBackwardArrN(1000))}`);
//console.log(`MargeSort with backward array ${perfomanceMergSort(createSortedBackwardArrN(1000))}`);
