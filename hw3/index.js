//Task 1.1
const products = [
  {name: "Apple", price: 12},
  {name: "Banana", price: 15},
  {name: "Orange", price: 11}
]
const calculateDiscountedPrice = (products, discount) => {
  try {

    if(products.length > 0 && discount > 0 &&  discount < 100){
      const productsDiscont = [];
      products.forEach(element => {
      let productDiscontPrice = element.price - element.price * (discount / 100);
      let productDiscont = {name: `${element.name}`, price: `${productDiscontPrice}`};
      productsDiscont.push(productDiscont);
      });
      return productsDiscont;
    }else{
      throw Error('Invalid arguments!')
    }
  } catch (error) {
    return error.message;
  }
  
}
//console.log(calculateDiscountedPrice(products, 120));

//Task 1.2
const calculateTotalPrice = (products) => {
  if(!products || products.length == 0) return Error("Wrong data!");

  let totalPrice = 0;
  products.forEach(element => {
    totalPrice += element.price;
  });
  return totalPrice;
}
//console.log(calculateTotalPrice(products));

//Task 2.1
const person = {firstName: "Ruslan", lastName: "Andreev"};

const getName = (person) => person.firstName ? person.firstName : Error("FirstName is not defined");
const getLastName = (person) => person.lastName ? person.lastName : Error("LastName is not defined"); 
const comp = (f1, f2) => value => (`${f1(value)} ${f2(value)}`);

const getFullName = comp(getName, getLastName);
try {
  console.log(getFullName(person));
} catch (error) {
  console.log(error.message);
}



//Task 2.2
const string = 'some string with some random words';

const spltFunctin = (arg) => arg.split(' ');
const sortFunction = (arg) => arg.sort();
const unicValues = (arg) => [... new Set(arg)];
const compose = (f1, f2, f3) => value => (f1(f2(f3(value))));

const filterUniqueWords = compose(unicValues, sortFunction, spltFunctin);
//console.log(filterUniqueWords(string))

//Task 2.3
const students = [
  {name: 'Alex', grade: [5, 6, 9, 7]},
  {name: 'Bob', grade: [3, 5, 4]},
  {name: 'Alice', grade: [6, 9, 5, 3]}
];

const getStudentGrade = (student) => {
  return student.grade.reduce((acc, grade) => acc + grade, 0) / student.grade.length;
}

const calculateAverageGrade = (students) => {
  return students.map(student => {
     const averGrade = getStudentGrade(student);
     return {
      ...student,
      grade: averGrade
     }
  });
}
//console.log(calculateAverageGrade(students));


//Task 3.1
function createCounter() {
  let count = 0;
  function counter() {
    return count++;
  }
  return counter;
}
const counter1 = createCounter();
const counter2 = createCounter();
//console.log(counter1());
//console.log(counter1());
//console.log(counter2());

//Task 3.2

const repeatFunction = (fn, num) => {
  try {

    if(!num) throw Error("Number is not defined or 0!");
    return function() {
      if(num < 0) {
        while(true){
          fn();
        }
      }else if(num > 0){
        for (let i = 0; i < num; i++) {
          fn();
        }
      } 
    }
    
  } catch (err) {
    throw err.message;
  }
  
}

const sayHi = () => {
  console.log("Hi!");
}
const repeat = repeatFunction(sayHi, 5);
try {
  repeat();
} catch (error) {
  console.log(error);
}


//Task 4.1
const calculateFactorial = (arg) => {
  if(!arg || typeof arg !== 'number') return Error("Invalid argument, enter number!");

  return arg > 1 ? arg * calculateFactorial(arg - 1) : arg;
}
//console.log(calculateFactorial(5));

//Task 4.2
const power = (base, exponent) => {
  if(exponent == 0) {
    return 1;
  }else {
    return base * power(base, exponent - 1);
  }
}
//console.log(power(4, 3));

//Task 5.1

const arrForMap = [2, 4, 5, 7];

const mapFn = (arg) => {
  try {
    if(typeof arg !== "number") throw Error("Argument is not a number!");

    return arg * 3;

  } catch (err) {
    return err.message;
  }
  
}
const lazyMap = (fn, arr) => {
  
  try {
    if(!fn || !arr || arr.length == 0) throw Error("Wrong arguments!");

    let index = 0;
  
    function iteration() {
      if (index >= arr.length) {
          return;
      }
        const value = fn(arr[index]);
        index++;
        return value;
      }

    return iteration;

  } catch (err) {
    throw err.message;
  }
}
const iterationFunction = lazyMap(mapFn, arrForMap);
try {
  console.log(iterationFunction());
  console.log(iterationFunction());
  console.log(iterationFunction());

} catch (err) {
  console.log(err.message);
}

//Task 5.2
const fibonacciGenerator = () => {
  let a = 0;
  let b = 1;

  return function() {
    const current = a;
    a = b;
    b = a + current;
    return current;
  }
}
const fibonacci = fibonacciGenerator();
//console.log(fibonacci());
//console.log(fibonacci());
//console.log(fibonacci());
//console.log(fibonacci());
//console.log(fibonacci());
