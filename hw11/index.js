//Check arguments
const checkArguments = (arg) => {
  if(!arg) throw new Error("Arguments is empty, enter data!")
}
//Stack
//Класс предстовляет собой реализацию структуры данных Stack с дополнительными методами
//getMin и getMax для поиска минимального и максимального значения
//конструктор решил не использовать т.к можно сразу указать свойства, и они не изменяются
//при создании экземпляра класса
class Stack {

  #stack = [];
  #minStack = [];
  #maxStack = [];

  push(data) {//метод push добавляет данные на вершину Stack и в массив min и max 
              //если они являются минимальным или максимальным значением и возвращакет Stack
    checkArguments(data);

    this.#stack.push(data);

    if (this.#minStack.length === 0 || data <= this.getMin()) {
      this.#minStack.push(data);
    }

    if (this.#maxStack.length === 0 || data >= this.getMax()) {
      this.#maxStack.push(data);
    }
    return this.#stack;
  }

  pop() {//метод Pop удаляет вершину из Stack (прицип LIFO) и возвращает это значение
    let data = this.#stack.pop();
    return data;
  }

  peek() {//Этот метод возвращает верхний элемент стека не изменяя его.
    let peekElement = this.#stack[this.#stack.length - 1];
    return peekElement;
  }

  isEmpty() {//метод возвращает булево значение, указывающее на то, пуст стек или нет.
    return !this.#stack.length;
  }

  getMin() {//метод возвращает минимальное значение в стеке. Он проверяет длину массива #minStack и возвращает последний элемент, если он не пуст.
            // если массив #minStack пуст, то возвращается значение undefined.
    if (this.#minStack.length === 0) {
      return undefined;
    }
  
    return this.#minStack[this.#minStack.length - 1];
  }
  
  getMax() {//метод возвращает максимальное значение в стеке. Он проверяет длину массива #maxStack и возвращает последний элемент, если он не пуст.
            // если массив #maxStack пуст, то возвращается значение undefined.
    if (this.#maxStack.length === 0) {
      return undefined;
    }
  
    return this.#maxStack[this.#maxStack.length - 1];
  }
}

const myStack = new Stack();
//console.log(myStack.push(3))
//console.log(myStack.push(1))
//console.log(myStack.push(7))
//console.log(myStack.push(9))
//console.log(myStack.push(0))

//console.log(myStack.getMin()); // Output: 0
//console.log(myStack.getMax()); // Output: 9
//console.log(myStack.pop());
//console.log(myStack.isEmpty())

//Класс Queue реализует структуру данных очереди, 
//используя объект в качестве базовой структуры данных.
//Я выбрал обьект т.к это позволит добавлять и удалять данные за O(1)
class Queue {//
              //
  #queue = {};
  frontIndex = 0
  backIndex = 0
  
  enqueue(data) {//добаляет обьект в конец очереди, испоьзуя инекс как ключ
    checkArguments(data);

    this.#queue[this.backIndex] = data;
    this.backIndex++;
    return this.#queue;
  }

  dequeue() {//удаляет обьект из начала очереди, испоьзуя инекс как ключ
            //в отличии от массива нет необходимости индексировать весь массив заново
            //сложность алгоритма O(1)
    let data = this.#queue[this.frontIndex];
    delete this.#queue[this.frontIndex];
    this.frontIndex++;
    return data;
  }

  peek() {//т.к это очередь то векршина это перевй элемент
          //возвращаем первый элемент
    return this.#queue[this.frontIndex];
  }

  isEmpty() {//проверяем если очередь пуста, то возвращаем true, если нет то False
              //проверяем есть ли ключи 
              
    return Object.keys(this.#queue).length === 0 ? true : false;
  }

}
const myQueue = new Queue();
console.log(myQueue.isEmpty())
console.log(myQueue.enqueue("some data"))
console.log(myQueue.enqueue("data"))
//console.log(myQueue.peek())
//console.log(myQueue.dequeue())
console.log(myQueue.isEmpty())

//класс который представляет ноду для связного списка
//данные и указатель на следующую ноду
class NodeForLinkedList {
  constructor(data, next = null) {
    this.data = data;
    this.next = next;
  }
}

//класс представляет LinkedList, изначально задаем head(по умолчанию null)
class LinkedList {
  head = null;
  //Метод принимает параметр data и добавляет новый узел с заданными данными в конец связанного списка. 
  //Если связный список пуст, то новый узел становится первым, или обходим связный кписок пока 
  //не достигнем последнего узла и возвращаем this
  inserting(data) {
    checkArguments(data)
    
    const newNode = new NodeForLinkedList(data);
    
    if (this.head === null) {
      this.head = newNode;
    } else {
      let current = this.head;
      while (current.next !== null) {
        current = current.next;
      }
      current.next = newNode;
    }
    
    return this;
  }
  //метод принимает параметр data и удаляет из связанного списка первое вхождение узла с заданными данными.
  //Если связный список пуст, то ничего не удаляется. 
  //Обходим linkedList и удаляем нужный узел, затем обновляем свойство next предыдущего узла, чтобы пропустить текущий узел.
   
  deleting(data) {
    checkArguments(data);
    if (this.head === null) {
      return;
    }
    
    if(this.head.data === data){
      this.head = this.head.next;
      return
    }
    let current = this.head;
    let previous = null;
    
    while (current !== null && current.data !== data) {
      previous = current;
      current = current.next;
    }
    
    if (current === null) {
      return;
    }
    
    previous.next = current.next;//меняем указатель next в предыдущем элементе на next из удаленного элемента

  }
  //метод принимает параметр data и ищет в связанном списке узел с заданными данными.
  //начинаем обход с головного узла, используя current и если находит то возвращает true
  searching(data) {
    checkArguments(data);
    let current = this.head;
    
    while (current !== null) {
      if (current.data === data) {
        return true;
      }
      current = current.next;
    }

    return false;
  }
  
  //метод проверяет, содержит ли связный список цикл. Метод начинает работу с двумя указателями, медленным и быстрым, 
  //которые первоначально указывают на головной узел. Указатель low перемещается на один шаг за раз, а указатель fast - на два шага за раз.
  //Если происходит цикл, то указатель fast догонит указатель low. 
  //Если указатель next указателя fast становится null, это означает, что в связном списке нет цикла. 
  //Dозвращаем true, если цикл есть
  
  hasCycle() {
    if (!this.head || !this.head.next) {//проверяем ели пустой список или элемент один
      return false;
    }
  
    let slow = this.head;
    let fast = this.head.next;
  
    while (slow !== fast) {
      if (!fast || !fast.next) {
        return false;
      }
      slow = slow.next;
      fast = fast.next.next;//добаляем шаг на два шага  
    }
    return true;
  }
}

const myLinkedList = new LinkedList();
myLinkedList.inserting('a')
myLinkedList.inserting('b')
myLinkedList.inserting('c')
myLinkedList.inserting('d')
//console.log(myLinkedList.searching('c'))
//myLinkedList.deleting('b')
//console.log(JSON.stringify(myLinkedList))
//case with cycle in LinkedList
//myLinkedList.head.next.next.next.next = myLinkedList.head.next;
//console.log(myLinkedList.hasCycle())


//класс представляет ноду для BinaryTree (данные, левый указатель и правый)
//леевый меньше чем нода, правый больше
class NodeForBinaryTree {
  constructor(data) {
    this.data = data;
    this.left = null;
    this.right = null;
  }
}

//Класс для создания Бинарного дерева

class BinaryTree {
  constructor() {
    this.root = null;
  }

  //Метод принимает параметр data и добаляет в бинарное дерево новый узел с заданными данными.
  //Если бинарное дерево пусто , то новый узел становится корнем бинарного дерева, иначе
  //вызывается метод insertHandler для рекурсивного поиска подходящей позиции для добавления
  //нового узла на основе сравнения данных с данными текущего узла.
  
  inserting(data) {
    checkArguments(data)
    const newNode = new NodeForBinaryTree(data);
    if(this.root === null) {
      this.root = newNode;
      return;
    }
    this.insertHandler(this.root, newNode);
  }

  insertHandler(currentNode, newNode) {
    //проверяем если новая нода меньше root то добаляем в левую часть
    if(newNode.data < currentNode.data) {//currentNode.data изначально корневой элемент с которым сравнивем новую ноду
      if(currentNode.left === null){
        currentNode.left = newNode;
      }else{
        this.insertHandler(currentNode.left, newNode);
      } 
    }
    //проверяем если новая нода больше root то добаляем в правую часть
    if(newNode.data > currentNode.data) {
      if(currentNode.right === null){
        currentNode.right = newNode;
      }else{
        this.insertHandler(currentNode.right, newNode);
      } 
    } 
  }

  searching(data) {
    checkArguments(data);
    return this.searchHandler(this.root, data);
  }
  
  //метод, который рекурсивно ищет в бинарном дереве узел с заданными данными. 
  //Он сравнивает данные текущего узла с заданными данными. Если они совпадают, то возвращается true. 
  //Если заданные данные меньше данных текущего узла, то рекурсивно вызывается searchHandler с левым дочерним узлом в качестве нового текущего узла. 
  //Если заданные данные больше данных текущего узла, то рекурсивно вызывается searchHandler с правым дочерним узлом в качестве нового текущего узла. 
  //Если текущий узел становится null, то возвращается `false`.
  
  searchHandler(currentNode, data) {
    
    if(currentNode === null) {
      return false;
    }

    if(currentNode.data === data) {
      return true;
    }

    if(data < currentNode.data) {
      return this.searchHandler(currentNode.left, data);
    }

    if(data > currentNode.data) {
      return this.searchHandler(currentNode.right, data);
    }
  }

  inorder() {
    const result = [];
    this.inorderHandler(this.root, result);
    return result;
  }

  //метод, выполняющий последовательный обход бинарного дерева. 
  //Он рекурсивно обращается к левому поддереву, добавляет данные текущего узла в массив `result`,
  //а затем рекурсивно обращается к правому поддереву

  inorderHandler(currentNode, result) {
    if (currentNode === null) {
      return;
    }
    
    this.inorderHandler(currentNode.left, result);//Левое поддерево 
    result.push(currentNode.data);
    this.inorderHandler(currentNode.right, result);//Правое поддерево
  }

  //метод проверяет бинарное ли дерево или нет
  isBST() {
    return this.bstHandler(this.root, -Infinity, Infinity)
  }
  
  //метод, который рекурсивно проверяет, образует ли данный узел и его потомки корректный BST. 
  //Он сравнивает данные узла со значениями min и max. 
  //Если данные узла меньше min или больше max, то возвращается `false`, так как првило нарушено. 
  //В противном случае рекурсивно вызывается bstHandler на левом дочернем узле с обновленными значениями min и node.data - 1 в качестве новых границ,
  //и на правом дочернем узле с node.data + 1 и max в качестве новых границ.
  //Если оба вызова возвращают true, то это означает, что дерево является корректным бинарным.
  
  bstHandler(node, min, max) {
    if (node === null) {
      return true;
    }

    if (node.data < min || node.data > max) {
      return false;
    }

    return (
      this.bstHandler(node.left, min, node.data - 1) &&
      this.bstHandler(node.right, node.data + 1, max)
    );
  }
}

const myBinaryTree = new BinaryTree();

// myBinaryTree.inserting(5)
// myBinaryTree.inserting(4)
// myBinaryTree.inserting(7)
// myBinaryTree.inserting(2)
// myBinaryTree.inserting(3)
// myBinaryTree.inserting(13)
// console.log(JSON.stringify(myBinaryTree))
// console.log(myBinaryTree.searching(12))
// console.log(myBinaryTree.inorder())
//console.log(myBinaryTree.isBST())

//класс для создания Graph. Граф представляет собой вершины и ребра которые их связывают
//у ребер может быть параматр weight  вданном случае он необходим для метода shortPathDijkstra
class Graph {
  graph = new Map();//создаем Graph на основе структуры данных Map
  //метод добавляет в граф новую вершину. При этом проверяется, существует ли уже данная вершина в графе,
  // с помощью метода has объекта Map. Если вершина не существует, то добавляется запись в graph с вершиной в качестве ключа
  // и пустым массивом в качестве значения.
  addVertex(vertex) {
    checkArguments(vertex);
    if(this.graph.has(vertex)) {
      return;
    }
    this.graph.set(vertex, []);
  }
  //Этот метод добавляет ребро между двумя вершинами графа. 
  //Сначала проверяется, существуют ли обе вершины в графе с помощью метода has. 
  //Если они существуют, то с помощью метода get массивы вершин.
  //В каждый массив добавляется объект, содержащий соседнюю вершину и вес ребра.
  addEdge(vertex1, vertex2, weight) {
    if(!vertex1 || !vertex2) throw new Error("Vertexies are underfined, enter data!")

    if (this.graph.has(vertex1) && this.graph.has(vertex2)) {
      const edge1 = this.graph.get(vertex1);
      const edge2 = this.graph.get(vertex2);

      edge1.push({ vertex: vertex2, weight });
      edge2.push({ vertex: vertex1, weight });
    }
  }

  dfs(vertex, destination) {
    if(!vertex) throw new Error("Enter element for start position!");
    if(!destination) throw new Error("Enter element for destination element!");

    const visited = new Set();
    return this.dfsHandler(vertex, destination, visited);
  }
  //рекурсивный вспомогательный метод для поиска в глубину. 
  //Он проверяет, является ли текущая вершина вершиной назначения, и возвращает true, если да. 
  //Он добавляет текущую вершину в набор visited и перебирает ее соседние вершины. Для каждой соседней вершины
  // рекурсивно вызывается dfsHandler. Если вызов возвращает true, это означает, что вершина достигнута и метод возвращает true.
  // Если все соседние вершины были посещены и ни один из вызовов не возвращает true, то метод возвращает false.
  dfsHandler(vertex, destination, visited) {
    if (vertex === destination) return true;

    visited.add(vertex);
    const edges = this.graph.get(vertex);

    for (let edge of edges) {
      if (!visited.has(edge.vertex)) {

        let reached = this.dfsHandler(edge.vertex, destination, visited);

        if (reached) {
          return true;
        }
      }
    }
    return false;
  }
  //метод выполняет поиск в ширину, начиная с заданной вершины, и проверяет, достижима ли из нее конечная вершина.
  //Создаем множество visited для отслеживания посещенных вершин и массив queue для хранения вершин. 
  //Работа начинается с добавления начальной вершины в очередь. Затем выполняется цикл, который продолжается до тех пор, пока очередь не станет пустой.
  //На каждой итерации цикл удаляет первую вершину из очереди, проверяет, является ли она вершиной назначения, и возвращает true, если является. 
  //Текущая вершина добавляется в набор `visited` и выполняется итерация по соседним вершинам. Для каждой соседней вершины, если она не была посещена ранее и еще не находится в очереди, добавляется в очередь.
  // Если цикл завершается, не найдя вершины назначения, метод возвращает false.
  bfs(vertex, destination) {
    if(!vertex) throw new Error("Enter element for start position!");
    if(!destination) throw new Error("Enter element for destination element!");

    const visited = new Set();
    const queue = [vertex];

    while (queue.length > 0) {
      const current = queue.shift();

      if (current === destination) {
        return true;
      }
      
      visited.add(current);

      const edges = this.graph.get(current);
      for (let edge of edges) {
        if (!visited.has(edge.vertex) && !queue.includes(edge.vertex)) {
          queue.push(edge.vertex);
        }
      }
    }
    return false;
  }

  //метод находит кратчайший путь между заданной вершиной и вершиной назначения, 
  //используя метод поиска в ширину. Создаем множество visited для отслеживания посещенных вершин и массив queue для хранения вершин,
  //подлежащих обработке. Очередь содержит пары вершин и путей к ним. Поиск начинается с добавления в очередь начальной вершины и пустого пути. 
  //Затем выполняется цикл, который продолжается до тех пор, пока очередь не станет пустой. На каждой итерации удаляется первая пара из очереди, 
  //проверяется, является ли текущая вершина вершиной назначения, и возвращается путь, если является. Текущая вершина добавляется в набор visited
  //и выполняется итерация по ее соседним вершинам. Для каждой соседней вершины, если она не была посещена ранее, в очередь добавляется пара: соседняя вершина и текущий путь.
  //Если цикл завершается без нахождения конечной вершины, то метод возвращает `null`.
  shortestPathBfs(vertex, destination) {
    if(!vertex) throw new Error("Enter element for start position!");
    if(!destination) throw new Error("Enter element for destination element!");

    const visited = new Set();
    const queue = [[vertex, []]];
  
    while(queue.length > 0){
      const [current, path] = queue.shift();
  
      if(current === destination) {
        return path.concat(current);
      } 
      visited.add(current)
  
      const edges = this.graph.get(current);
      //console.log(edges)
      for(let edge of edges) {
        if(!visited.has(edge.vertex)) {
          queue.push([edge.vertex, path.concat(current)]);
        }
      }
    }
    return null;
  }
  //метод находит кратчайший путь между заданными начальной и конечной точками по алгоритму Дейкстры.
  //Создается структура Map(distances) для хранения кратчайших расстояний от начальной точки до каждой вершины,
  // структура Map(previous) для хранения предыдущей вершины на кратчайшем пути и массив queue для хранения вершин,
  //В начале работы задается расстояние до начальной точки, равное 0, и расстояния до всех остальных вершин, равные бесконечности. 
  //Также устанавливается предыдущая вершина всех вершин в null. Затем все вершины добавляются в очередь.
  //Далее выполняется цикл, который продолжается до тех пор, пока очередь не станет пустой. На каждой итерации находится вершина с минимальным расстоянием
  // от начальной точки очереди. Если эта вершина является конечной, возвращается кратчайший путь, проходящий по предыдущим вершинам от конечной до начальной точки.
  // если нет, то вершина удаляется из очереди и выполняется итерация по ее соседним вершинам.
  // Для каждой соседней вершины вычисляется расстояние от начальной точки через текущую вершину и обновляется расстояние и предыдущая вершина, если вычисленное расстояние меньше текущего.
  // Если цикл завершается, не найдя конечной точки, метод возвращает null.
  shortPathDijkstra(startPoint, endPoint) {
    if(!startPoint) throw new Error("Enter element for startPoint position!");
    if(!endPoint) throw new Error("Enter element for endPoint element!");

    const distances = new Map();
    const previous = new Map();
    const queue = [];

    for (let vertex of this.graph.keys()) {
      if (vertex === startPoint) {
        distances.set(vertex, 0);
      } else {
        distances.set(vertex, Infinity);
      }
      previous.set(vertex, null);
      queue.push(vertex);
    }

    while (queue.length > 0) {
      
      let minDistance = Infinity;
      let minVertex = null;
      for (let vertex of queue) {
        if (distances.get(vertex) < minDistance) {
          minDistance = distances.get(vertex);
          minVertex = vertex;
        }
      }

      if (minVertex === endPoint) {
        
        const path = [];
        let current = endPoint;
        while (current !== null) {
          path.unshift(current);
          current = previous.get(current);
        }
        return path;
      }

      queue.splice(queue.indexOf(minVertex), 1);

      const edges = this.graph.get(minVertex);
      for (let edge of edges) {
        const distance = minDistance + edge.weight;
        if (distance < distances.get(edge.vertex)) {
          distances.set(edge.vertex, distance);
          previous.set(edge.vertex, minVertex);
        }
      }
    }

    return null;
  }

}

const myGraph = new Graph();

myGraph.addVertex("Alex")
myGraph.addVertex("Bob")
myGraph.addVertex("Kate")
myGraph.addVertex("Jack")
myGraph.addVertex("Anna")

//console.log(myGraph)

myGraph.addEdge("Alex", "Kate", 3);
myGraph.addEdge("Alex", "Bob", 2);
myGraph.addEdge("Jack", "Bob", 5);
myGraph.addEdge("Anna", "Bob", 4);

//console.log(myGraph)
//console.log(myGraph.dfs("Alex", "Kate")) //returns true if reachable
//console.log(myGraph.bfs("Alex", "Bob")) //returns true if reachable
//console.log(myGraph.shortestPathBfs("Alex", "Kate")) //shortest path
//console.log(myGraph.shortestPathBfs("Alex", "Anna")) //shortest path
//console.log(myGraph.shortPathDijkstra("Alex", "Anna"));