//Task1
const person = {
  firstName: "John",
  lastName: "Doe",
  age: 30,
  email: "john.doe@example.com",
}
// Set descriptors to object
Object.keys(person).forEach(key => {
  Object.defineProperty(person, key, {
    value: person[key],
    writable: false,
    enumerable: true,
    configurable: true
  });
});

Object.defineProperty(person, 'adress', {
  value: {},
  enumerable: false,
  configurable: false
});
const newInfo = {
  firstName: "Alex",
  lastName: "Petrov",
  age: 24,
}

person.updateInfo = function(newInfo) {
  Object.keys(newInfo).forEach(property => {
    
    if(person.hasOwnProperty(property) && Object.getOwnPropertyDescriptor(person, property).writable){
      Object.defineProperty(person, property, {
        value: newInfo[property],
        writable: false,
        configurable: true,
      });
    }
  })
    
}
person.updateInfo(newInfo)
//console.log(Object.getOwnPropertyDescriptor(person, 'adress'))
//console.log(person)

//Task2
const product = {
  name: "Laptop",
  price: 1000,
  quantity: 5,
}
Object.defineProperties(product, {
  name: {value: "Laptop", configurable: true},
  price: {value: 1000, writable: false, enumerable: false},
  quantity: {value: 5, writable: false, enumerable: false },
});

function getTotalPrice(product) {
  let price = Object.getOwnPropertyDescriptor(product, 'price').value;
  let quantity = Object.getOwnPropertyDescriptor(product, 'quantity').value;
  return price * quantity;
}
function deleteNonConfigurable(obj, key) {
  
  let prop = Object.getOwnPropertyDescriptor(obj, key);
  
  if(!prop) return new Error("No property!");
  if(prop.configurable === false) return new Error("None configurable!");
  delete obj[key];

  return obj;
  
}

//console.log(getTotalPrice(product));
//console.log(deleteNonConfigurable(person, 'firstName'));

//Task3

const bankAccount = {
  balance: 1000,

  get formattedBalance() {
    return `$${this.balance}`;
  },

  set formattedBalance(value) {
    this.balance = value;
  }

}

const bankAccount2 = {
  balance: 2000,

  get formattedBalance() {
    return `$${this.balance}`;
  },

  set formattedBalance(value) {
    this.balance = value;
  }

}

function transfer(curr, target, amaunt) {
  let currentCash = +curr.formattedBalance.slice(1);
  let targetCash = +target.formattedBalance.slice(1);
  
  curr.formattedBalance = currentCash - amaunt;
  target.formattedBalance = targetCash + amaunt;

  console.log(bankAccount.formattedBalance)
  console.log(bankAccount2.formattedBalance)
}
transfer(bankAccount, bankAccount2, 200)

//console.log(bankAccount.formattedBalance)
//bankAccount.formattedBalance = 500;
//console.log(bankAccount.formattedBalance)

//Task4

function createImmutableObject(obj) {
  const resultObj = {};

  for (const key in obj) {
    if( typeof obj[key] === "object" && obj[key] != null) {
      resultObj[key] = createImmutableObject(obj[key]);
    }else{
      Object.defineProperty(resultObj, key, {
        value: obj[key],
        writable: false,
        enumerable: true,
        configurable: true
      });
    }
  }
  return resultObj;
}
//const imutabeObject = createImmutableObject(person);


//Task5
function observeObject(obj, callback) {
  return new Proxy(obj, {
    get(target, prop) {
      callback(prop, 'get');
      return target[prop];
    },
    set(target, prop, value) {
      callback(prop, 'set');
      target[prop] = value;
      return true;
    },
    deleteProperty(target,prop){
      callback(prop, 'delete');
      delete target[prop];
      return true;
    }
  });
}

const proxy = observeObject(person, (prop, action) => {
  console.log(`Property ${prop} was ${action} on the object.`);
});
//proxy.name = "bob"
//proxy.name
//delete proxy.name

//Task6

function deepCloneObject(obj) {
  if(typeof obj !== "object") {
    return obj
  }
  const coppyObject = {}

  return Object.keys(obj).reduce((acc, key) => {
    acc[key] = deepCloneObject(obj[key]);
    return acc;
  }, coppyObject);

}
const coppyPerson = deepCloneObject(person);

console.log(coppyPerson)
coppyPerson.firstName = "Ivan";
console.log(coppyPerson)
console.log(person)


//Task7
const student = {
  name: "Alex",
  age: 23,
  phone: "+375 66 666 66 66",
  pet: true
}
const student2 = {
  name: "Alex",
  age: 22,
  pet: false
}
const student3 = {
  name: "Bob",
  age: "33",
  phone: "+375 66 666 66 66",
  pet: true
}
const schema = {
  name: { type: 'string', required: true },
  age: { type: 'number', required: true },
  phone: { type: 'string', required: true }, 
}
function validateObject(obj, schem) {
  for(let key in schem){

    if(schem[key].required && !obj.hasOwnProperty(key)){
      throw new Error("Now requide properties!");
    }
  }
  for(let key in obj){
    
    if(schem[key] && typeof obj[key] !== schem[key].type){
      throw new Error("Wrong type of data!");
    }
  }


  return 'Ok'
}
//console.log(validateObject(student, schema))
//console.log(validateObject(student2, schema))
//console.log(validateObject(student3, schema))


