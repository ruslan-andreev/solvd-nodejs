//JSON поддерживает следующие типы данных (объекты, массивы,примитивы:
//строки,числа, логические значения true/false, null).
//Преобразование обьекта в формат JSON следующим образом:
//Имена свойст оборачиваются в двойные кавычки("key")
//Значения ключа, если строка, то также оборачиваются в двойные кавычки
//Значения типа number и логические значения true/false, а так же null не изменяются.
//Методы обьектов и свойства со значением undefined пропускаются.


function myJSONParse(jsonString) {
  if (!jsonString) throw new Error("Enter value, empty argument");
  if (typeof jsonString !== "string") throw new Error("Wrong argumet, enter string");
  if (jsonString.length === 0) throw new Error("Wrong argumet, empty string");

  let currentIndex = 0;//текущая позиция в строке

  //фугкция счетчик обновляющая позицию поиска
  function nextIndex() {
    return ++currentIndex;
  }
  //функция пропускает пустую строку и пренос строки и обновляет счетчик текущей позиции
  function skip() {
    while (jsonString[currentIndex] === ' ' || jsonString[currentIndex] === '\n') {
      nextIndex();
    }
  }
  //обраюотчик типов, вызывает нужный обработчик
  function handlerTyps() {
    
    if (jsonString[currentIndex] === "{") {//для обьектов
      return handlerObject();
    } else if (jsonString[currentIndex] === "[") {//для массивов
      return handlerArray();
    } else if (jsonString[currentIndex] === '"') {//для строк (ключи обьектов и значения обьектов ели строка)
      return handlerString();
    } else if (/[0-9-]/.test(jsonString[currentIndex])) {//для цифр, чисел и экстонент
      return handlerNumber();
    } else {
      return handlerBooleanAndNull();//для false,true,number
    }
  }
  //обработчик строки
  function handlerString() {
    //регулярное выражение `stringRegexp`, которое ищет подстроку, заключенную в двойные кавычки
    //`.*?` означает любые символы, включая пустую строку
    //aлаг `y` в регулярных выражениях, позволяет начать поиск с указанной позиции, заданной свойством `lastIndex`.
    const stringRegexp = /".*?"/y;
    //Устанавливается значение свойства `lastIndex` регулярного выражения, указывающее на текущую позицию
    // в исходной строке, это позволяет начать поиск с определенной позиции в строке
    stringRegexp.lastIndex = currentIndex;
    //Выполняется метод `exec()` регулярного выражения, который ищет совпадение с шаблоном
    //начиная с позиции, указанной в `lastIndex`.
    //Результатом является массив `stringMatch`, содержащий найденное совпадение или `null`, если совпадение не найдено.
    const stringMatch = stringRegexp.exec(jsonString);
    if (stringMatch) {
      currentIndex = stringRegexp.lastIndex;
      //возвращаем найденую строку без кавычек
      return stringMatch[0].substring(1, stringMatch[0].length - 1);
    } else {
      throw new Error("Invalid JSON");
    }
  }

  function handlerObject() {
    let newObject = {};
    nextIndex();
    skip();
    //если следующий символ обозначает "}" это значит пустой обьек, возвращаем пустой обьект
    if (jsonString[currentIndex] === "}") {
      nextIndex();
      return newObject;
    }

    while (true) {
      skip();
      //следующий символ должен быть ключ обьекта и начинать с я кавычек
      if (jsonString[currentIndex] !== '"') {
        throw new Error("Invalid JSON");
      }
      //вызываем обработчик строки для key
      let objectKey = handlerString();
      skip();
      //следующий символ после ключа должен быть ":" если нет, то ошибка если есть, то пропускае его
      if (jsonString[currentIndex] !== ":") {
        throw new Error("Invalid JSON");
      }
      nextIndex();
      skip();
      //обработчик для value в обьекте
      let objectValue = handlerTyps();
      newObject[objectKey] = objectValue;
      skip();
      //после пары key: value должна идти "," или закрывающая скобка для обьекта
      if (jsonString[currentIndex] !== ",") {
        if (jsonString[currentIndex] === "}") {
          nextIndex();
          break;
        } else {
          throw new Error("Invalid JSON");
        }
      } else {
        nextIndex();
      }
    }
    return newObject;
  }

  function handlerArray() {
    let newArray = [];
    nextIndex();
    //если "]", то пустой массив возвращаем его
    if (jsonString[currentIndex] === "]") {
      nextIndex();
      return newArray;
    }
    while (true) {
      skip();
      newArray.push(handlerTyps());
      skip();
      //в массиве после каждого значения должна быть запятая или "]" конец массива
      if (jsonString[currentIndex] !== ",") {
        if (jsonString[currentIndex] === "]") {
          nextIndex();
          break;
        } else {
          throw new Error("Invalid JSON");
        }
      } else {
        nextIndex();
      }
    }
    return newArray;
  }

  //функция обработки числовых значений
  function handlerNumber() {
    
    // `[-+]?` - Опциональный символ "+" или "-" в начале числа, позволяющий указывать положительное или отрицательное число.
    // `\d+` - Один или более символов, представляющих цифры.
    // `\.?` - Опциональная точка, позволяющая указывать числа с плавающей точкой.
    // `\d*` - Ноль или более символов, представляющих цифры после точки.
    // `(?:[eE][-+]?\d+)*` - Опциональная группа, позволяющая указывать числа в экспоненциальной форме. 
    // `[eE]` - Символ "e" или "E", обозначающий экспоненциальное значение.
    // `\d+` - Один или более символов, представляющих цифры экспоненциального значения.
    
    const numberRegexp = /([-+]?\d+\.?\d*(?:[eE][-+]?\d+)*)/y;
    numberRegexp.lastIndex = currentIndex;
    const numberMatch = numberRegexp.exec(jsonString);
    if (numberMatch) {
      currentIndex = numberRegexp.lastIndex;
      return parseFloat(numberMatch[0]);
    } else {
      throw new Error("Invalid JSON");
    }
  }

  //функция обработчик false, true, null
  function handlerBooleanAndNull() {
    const valueRegexp = /true|false|null/y;
    valueRegexp.lastIndex = currentIndex;
    let valueMatch = valueRegexp.exec(jsonString);
    if (valueMatch) {
      currentIndex = valueRegexp.lastIndex;
      const value = valueMatch[0];
      if (value === "true") {
        return true;
      } else if (value === "false") {
        return false;
      } else if (value === "null") {
        return null;
      }
    } else {
      throw new Error("Invalid JSON");
    }
  }

  return handlerTyps();
}

let jsonString = `{
  "id": "647ceaf3657eade56f8224eb",
  "index": 10,
  "negativeIndex": -10,
  "anEmptyArray": [],
  "notEmptyArray": [1, 2, 3, "string", true, null],
  "boolean": true,
  "nullValue": null,
  "nestedObject": {
    "nestedString": "Hello World",
    "nestedNumber": 42,
    "nestedArray": [true, false]
  },
  "complexArray": [
    {
      "name": "Alice Alice",
      "age": 28,
      "hobbies": ["Reading", "Painting"]
    },
    {
      "name": "Bob Bob",
      "age": 32,
      "hobbies": ["Gaming", "Cooking"]
    }
  ]
}`;

console.log(myJSONParse(jsonString));