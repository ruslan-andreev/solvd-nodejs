const fs = require('fs')

class AsyncOperationManager {

  simulateAsyncOperation(delay) {
    setTimeout(() => {
      console.log(`Async operation completed after ${delay} ms`);
    }, delay);
  }

  scheduleImmediate() {
    setImmediate(() => {
      console.log("Immediate task executed");
    });
  }

  nextTick() {
    process.nextTick(()=> console.log("NextTick operation"))
  }
  
  readFile() {
    fs.readFile(__filename, ()=>{
      console.log("read file")
      setImmediate(() => console.log("SetImmediat in read file"))
      process.nextTick(() => console.log("nextTick in read file"))
    })
  }

  methodPromise() {
    return Promise.resolve().then(()=>console.log("Promise"))
  }
}

const manager = new AsyncOperationManager();

console.log("start") //1 выполнится первымвым т.к находится в основном потоке кода

manager.simulateAsyncOperation(200); //10 асинхронная операция будет помещена в Timer queue и ожидать выполнения
                              //пока не освободится очередь микротаск к моменту итечения таймера
                              //в данном примере ваполнтся последним

process.nextTick(() => {
  console.log("Microtask executed immediately"); //2 выпонится вторым т.к NextTickQueue имеет наибольший приоритет
                                              // и выполняется в текущем стеке вызова
});

manager.scheduleImmediate();  //6 Node.js не гарантирует выполннение до запланированного таймера
                              //но до следующей итерации eventloop

manager.simulateAsyncOperation(0) // 5 асинхронная операция будет помещена в Timer queue и ожидать выполнения
                                  //пока не освободится очередь микротаск т.к таймер 0мс выполнится раньше 
                                  //чем таймер со значением 200мс но после nextTick и промисов

manager.nextTick() //3 метод nextTick вызывает process.nextTick() и помещает его в очередь NextTick, где
                    //уже ожидает предыдущий NextTick, и будет вызван после него

manager.readFile() //7 после чтения файла вызывается callback и отправляет console.log() в CallStack т.к
                  //очередь NextTick уже пуста и промис выполнен выведет консоль
                  //после вызывается setImmediate помещается в check,
                  //следующий nextTick отправляется в nextTickQueue
                  //к этому моменту в eventLoop:
                  //NextTickQueue содерхит (nextTick in read file),Timer (таймер на 200 мс),Check (SetImmediat in read file)
                  
                  // 8 выполнится (nextTick in read file) очередь nextTick 
                  // 9 SetImmediat in read file
                  //10 Async operation completed after 200 ms

manager.methodPromise() //4 метод вызывает Promise и помещается в microTask queue приоритет ниже чем у nextTick
                        //но выше чем у таймеров и I/O операций. В первую очередь будут выполнены два 
                        //nextTick а после Promise                    