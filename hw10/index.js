class Book {
  constructor(title, author, isbn, price, availability) { //класс Book и конструктор в котором определяем 
    this.title = title;                                   //свойства при создании экземпляра класса
    this.author = author;
    this.isbn = isbn;
    this.price = price;
    this.availability = availability;
  }
  showTypeOfBook(){                   //метод showTypeOfBook представляет собой пример 
    return `This is a generic book.`  //полиморфизма т.к при наследовании переопределяется
  }                                   //и возвращает иной результат, в зваисимосьт от того в каком 
}                                     //экземпляре класса будет вызван

// класс FictionBook наследуется от Book, при наследовании используем super() что позволяут наслtдовать 
// constructor от родительского класса. Т.о нам не приходится инициализировать повторяющиеся свойства 
class FictionBook extends Book {
  #type = "fiction";             //свойство я решил создать приватным, это значит,
                                //что его невозможно изменить извне, или в экземпляре класса
                                //таким образом реализуется принцип инкапсуляции
  constructor(title, author, isbn, price, availability) {
    super(title, author, isbn, price, availability)//вызывая метод super мы вызываем конструктор
                                                  //родителького класса и наследуем его св-ва и методы
  }
  showTypeOfBook(){
    return `This is a ${this.#type} book.`
  }
}
class NoFictionBook extends Book {
  #type = "no-fiction";
  constructor(title, author, isbn, price, availability) {
    super(title, author, isbn, price, availability)
  }
  showTypeOfBook(){
    return `This is a ${this.#type} book.`;
  }
}

//класс User, в конструкторе которого определяем св-ва необходимые 
//при создании экземпляра класса
//метод createCart() создает экземпляр карзины со свойством указывающим 
//кому принадлежит корзина
class User {
  constructor(name, email, userId) {
    this.name = name;
    this.email = email;
    this.userId = userId;
  }
  createCart() {
    return new Cart(this.name);
  }
}

class Cart {
  
  cart = [];
  #totalPrice = null; //приватное св-во т.к не должно быть доступа извне
                      //и невозможно было изменить или подменить стоимость 
  constructor(name) {
    this.usersCart = name;
  }
  addBooks(books) {
    if(!books) throw new Error("Data is undefined!");
    if(!books.length || !Array.isArray(books)) throw new Error("Order is empty or wrong data type!");

    books.forEach(book => {
      this.cart.push(book);
    });
  }
  removeBook(isbn) {
    if(!isbn) throw new Error("ISBN number is undefined!");
    if(typeof isbn !== 'number' || Number.isNaN(isbn)) throw new Error('Wrong data type, enter number');

    this.cart = [...this.cart.filter(book => book.isbn !== isbn)];
  }
  clearCart() {
    this.cart.length = 0;
  }
  totalPrice() { //стоимость товаров которые .availability === true
    if(this.cart.length === 0) throw new Error("Cart is empty")
    for(let book of this.cart) {
      if(book.availability === true){
        this.#totalPrice += book.price;
      }
    }
    return this.#totalPrice;
  }
  getCart() {
    return this.cart;
  }

}

//наследуем заказ от корзины и наследуем св-ва и методы
//в данном примере использовал наследуемый метод totalPrice(), т.к он 
//необходим при расчете заказа и не придется его создавать заново
class Order extends Cart{
  #discount = 15; // приватное свойство т.к не должно быть доступа из вне
                // и невозможно было изменить или подменить скидку
  constructor(user, books) {
    super()
    this.cart = books;
    this.userOrder = user.name;
    this.userId = user.userId;
    this.booksOrdered = this.setOrderedBooks(books);
    this.cartPrice = this.totalPrice();
    this.orderPrice = this.getOrderedPrice();
  }
  getOrderedPrice() {
    let price = 0;
    this.booksOrdered.forEach((elem) => {
      price += elem.price;
    })
    return price - (price / 100 * this.#discount)
    
  }
  setOrderedBooks(books) {
    if(!books) throw new Error("Data is undefined!");
    if(!books.length || !Array.isArray(books)) throw new Error("User cart is empty or wrong data type!");

    return [...books.filter(elem => elem.availability === true)]
  }
  showOrderedBooks() {
    return this.booksOrdered;
  }
  showCartPrice() {
    return this.cartPrice;
  }
  showOrderedPrice() {
    return this.orderPrice;
  }
  showOrderInfo() {
    return `Ordered by: ${this.userOrder}, ID: ${this.userId}, totalPrice: ${this.orderPrice} with ${this.#discount}% discount`;
  }
  
}


const userAlex = new User("Alex", 'some@gmail.com', 1234);//создаем экземпляр от User
const userAnna = new User("Anna", 'anna@gmail.com', 3273);//и определяем нужные значени для конструктора

//создаем экземпляр книги и определяем нужные значени для конструктора
const someFictionBook = new FictionBook('someFictionBook', 'someAuthor', 13245465, 10, true)
console.log(someFictionBook)
console.log(someFictionBook.showTypeOfBook())
const someFictionBook2 = new FictionBook('someFictionBook2', 'someAuthor2', 4628375, 9, true)
//console.log(someFictionBook2)
const someNoFictionBook3 = new NoFictionBook(' someNoFictionBook3', 'someAuthor3', 9023283, 12, false)
//console.log(someNoFictionBook3)
const someGenerikBook = new Book(' someGenericBook', 'someAuthor4', 7134023, 7, true)
//console.log(someGenerikBook)
//console.log(someGenerikBook.showTypeOfBook())

let books = [someFictionBook, someFictionBook2, someNoFictionBook3, someGenerikBook]


const cartAlex = userAlex.createCart()// у эземпляра класса userAlex вызываем метод который создает
                                      //экземпляр корзины со свойством this.usersCart = name; в конструкторе
                                      //где сразу определяем кому принадлежит корзина
                                        
cartAlex.addBooks(books) //вызываем методы для корзины
//console.log(cartAlex.getCart())
//console.log(cartAlex.totalPrice())
//cartAlex.removeBook(13245465)
//console.log(cartAlex.getCart())

const orderByAlex = new Order(userAlex, cartAlex.getCart()) //формируем заказ создавая экземпляр класса
                                                            //и передаем в конструктор пользователя и его список
                                                            //покупок(считаем стоимость и итоговый зака только
                                                            //тех книг которые availability === true)
//console.log(orderByAlex)
console.log(orderByAlex.showCartPrice()) //стоимость товаров карзине наследуемый метод
console.log(orderByAlex.showOrderedPrice()) //стоимость закакза со скидкой
console.log(orderByAlex.showOrderInfo()) //информация о заказе