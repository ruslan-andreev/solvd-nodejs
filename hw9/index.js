//Error handling functions
const checkArrayParams = (data) => {
  if(!data) throw new Error('Data is udefined!');
  if(!Array.isArray(data)) throw new Error('Wrong data type!');
  if(data.length == 0) throw new Error('Array is empty!');
}

const checkNumberParams = (value) => {
  if(typeof value !== 'number' || Number.isNaN(value)) throw new Error('Wrong data type, enter number');
  if(!value) throw new Error('Value is udefined!');
}

const checkCallbackParams = (callback) =>{
  if(!callback) throw new Error('Data is udefined!');
  if(typeof callback !== "function") throw new Error('Callback is not a function!');
}

//Task 1

const promises = [
  Promise.resolve(1),
  Promise.resolve(2),
  Promise.resolve(3),
  //Promise.reject('Error')
];

// Alexandrina chack case
//const delay = (ms, value) => new Promise(res => setTimeout(() => res(value), ms));
//const promises = [  delay(3000, 'a'),  delay(1000, 'b'),  delay(2000, 'c')];
//promiseAll(promises).then(results => { console.log(results); });
//
function promiseAll(promises) {
  checkArrayParams(promises);

  return new Promise((resolve, reject) => {
    const results = [];

    for(let i = 0; i < promises.length; i++){
      const currentPromise = promises[i];
      
      currentPromise
        .then(response => {
          results[i] = response;
          if(results.filter(elem => !!elem).length === promises.length){
            resolve (results);
          }
        })
        .catch(error => {
          reject(error);
        })
    }

  })
}

promiseAll(promises)
  .then(results => {
    console.log("All promises resolved:", results); // Expected: [1, 2, 3]
  })
  .catch(error => {
    console.error("At least one promise rejected:", error);
  });


//Task 2
const promisesTask2 = [
  Promise.resolve(1),
  Promise.reject("Error occurred"),
  Promise.resolve(3)
];
  
function promiseAllSettled(promises) {
  checkArrayParams(promises);

  return new Promise((resolve, reject) => {
    const results = [];
    for(let i = 0; i < promises.length; i++){
      const respObj = {};
      const currentPromise = promises[i];

      currentPromise
        .then(response => {
          respObj.status = "fulfilled";
          respObj.value = response;
          results[i] = respObj;
        
          if(results.filter(elem => !!elem).length === promises.length){
            resolve (results);
          }
        })
        .catch(error => {
          respObj.status = 'rejected';
          respObj.reasone = error;
          results[i] = (respObj)
          
          if(results.filter(elem => !!elem).length === promises.length){
            resolve (results);
          }
        })
    }
  })
};

promiseAllSettled(promisesTask2)
  .then(results => {
    console.log("All promises settled:", results);
      // Expected: [{ status: 'fulfilled', value: 1 },
      //            { status: 'rejected', reason: 'Error occurred' },
      //            { status: 'fulfilled', value: 3 }]
});

//Task 3

function asyncFunction1() {
  return Promise.resolve("Result from asyncFunction1");
}
  
function asyncFunction2(data) {
  return Promise.resolve(data + " - Result from asyncFunction2");
}
  
function asyncFunction3(data) {
  return Promise.resolve(data + " - Result from asyncFunction3");
}

const functionsArray = [asyncFunction1, asyncFunction2, asyncFunction3];

function chainPromises(functionsArray) {
  checkArrayParams(functionsArray);

  return new Promise((resolve, reject) => {

     let result = Promise.resolve()
    
     functionsArray.forEach(elem => {
      let currentFn = elem

      result = result.then(resp => {
        return currentFn(resp)
      })

     })
    resolve(result)
  }) 
}

chainPromises(functionsArray)
  .then(result => {
    console.log("Chained promise result:", result);
    // Expected: "Result from asyncFunction1 - Result from asyncFunction2 - Result from asyncFunction3"
  })
  .catch(error => {
    console.error("Chained promise error:", error);
  });

//Task 4

function callbackStyleFunction(value, callback) {
  checkNumberParams(value);
  checkCallbackParams(callback);
  setTimeout(() => {
    if (value > 0) {
      callback(null, value * 2);
    } else {
      callback("Invalid value", null);
    }
  }, 1000);
}

function promisify(callback) {
  checkCallbackParams(callback);
  return (arg) => {
    checkNumberParams(arg);

    return new Promise((resolve, reject) => {
      
      callback(arg, (err, res)=>{
        if(!err){
          resolve(res)
        }else{
          reject(err)
        }
      }) 
    });
  };
}

const promisedFunction = promisify(callbackStyleFunction);

promisedFunction(3)
  .then(result => {
    console.log("Promised function result:", result); // Expected: 6
  })
  .catch(error => {
    console.error("Promised function error:", error);
  });
